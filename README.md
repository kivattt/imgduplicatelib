# imgduplicatelib

imgduplicatelib - Check how similar two images are

`double imgduplicatelib::get_similarity_percent(sf::Image img1, sf::Image img2)`

Returns similarity between two images (double 0 to 1) 0 being nothing like eachother
 1 being the exact same image

`sf::Image imgduplicatelib::get_resized(const sf::Image img, const sf::Vector2u newSize)`

Returns resized (nearest neighbor) image

`sf::Color imgduplicatelib::get_average_colors(vector <sf::Color> colors)`

Returns average r, g, b values from list of colors