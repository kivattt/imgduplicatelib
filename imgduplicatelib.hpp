#ifndef IMGDUPLICATELIB_HPP
#define IMGDUPLICATELIB_HPP

#include <SFML/Graphics.hpp>
#include <vector>

#define MAX(A,B) ((A) > (B) ? (A) : (B))
#define MIN(A,B) ((A) < (B) ? (A) : (B))
#define DIFF(A,B) (std::abs(((A) > (B)) ? ((A) - (B)) : ((B) - (A))))

using std::vector;

namespace imgduplicatelib{
	sf::Image get_resized(const sf::Image img, const sf::Vector2u newSize){
		sf::Image out;
		out.create(newSize.x, newSize.y);

		const double widthRatio = double(img.getSize().x) / newSize.x;
		const double heightRatio = double(img.getSize().y) / newSize.y;

		for (unsigned y=0; y<newSize.y; y++){
			for (unsigned x=0; x<newSize.x; x++)
				out.setPixel(x, y, img.getPixel(x * widthRatio, y * heightRatio));
		}

		return out;
	}

	sf::Color get_average_colors(vector <sf::Color> colors){
		unsigned r=0,g=0,b=0;
		for (sf::Color c : colors){
			r += c.r;
			g += c.g;
			b += c.b;
		}

		const double s = colors.size();

		return sf::Color(r/s, g/s, b/s);
	}

	double get_similarity_percent(sf::Image img1, sf::Image img2){
		// Resize images to common size
		sf::Vector2u newCommonImgSize;
		if (img1.getSize().x < img2.getSize().x)
			newCommonImgSize.x = img1.getSize().x;
		else
			newCommonImgSize.x = img2.getSize().x;

		if (img1.getSize().y < img2.getSize().y)
			newCommonImgSize.y = img1.getSize().y;
		else
			newCommonImgSize.y = img2.getSize().y;

		img1 = get_resized(img1, newCommonImgSize);
		img2 = get_resized(img2, newCommonImgSize);

		double out=0;
		for (unsigned y=0; y<newCommonImgSize.y; y+=2){
			for (unsigned x=0; x<newCommonImgSize.x; x+=2){
				sf::Color avgColorImg1 = get_average_colors(vector <sf::Color> {img1.getPixel(x,y), img1.getPixel(x+1,y), img1.getPixel(x,y+1), img1.getPixel(x+1,y+1)});
				sf::Color avgColorImg2 = get_average_colors(vector <sf::Color> {img2.getPixel(x,y), img2.getPixel(x+1,y), img2.getPixel(x,y+1), img2.getPixel(x+1,y+1)});

				out += double(DIFF(avgColorImg1.r, avgColorImg2.r)) / (255*3) / 3;
				out += double(DIFF(avgColorImg1.g, avgColorImg2.g)) / (255*3) / 3;
				out += double(DIFF(avgColorImg1.b, avgColorImg2.b)) / (255*3) / 3;
			}
		}

		return 1 - (out / ((newCommonImgSize.x/2) * (newCommonImgSize.y/2)));
	}
}

#endif // IMGDUPLICATELIB_HPP
